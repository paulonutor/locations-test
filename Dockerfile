FROM node:16.14.2-alpine3.15

WORKDIR /app

COPY package.json package-lock.json ./

RUN npm config set unsafe-perm true
RUN npm install

# fix permissions of node_modules folder
RUN chown -R node:node /app/node_modules

COPY . .

USER node

EXPOSE 3000

ENV CHOKIDAR_USEPOLLING=true

CMD ["npm", "start"]