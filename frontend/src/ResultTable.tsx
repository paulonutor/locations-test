import { Location } from "./types";

interface ResultTableProps {
  items: Location[];
}

function ResultTable({ items }: ResultTableProps) {
  return (
    <table>
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        {items.length ? (
          items.map((item, idx) => (
            <tr key={idx + 1}>
              <td>{item.id || ""}</td>
              <td>{item.name}</td>
            </tr>
          ))
        ) : (
          <tr>
            <td
              colSpan={2}
              style={{
                textAlign: "center",
                fontWeight: "bold",
                color: "darkgray",
              }}
            >
              No results were found
            </td>
          </tr>
        )}
      </tbody>
    </table>
  );
}

export default ResultTable;
