import { useCallback, useRef, useState } from "react";

import { LocationResult, Location } from "./types";

import SearchBar from "./SearchBar";
import ResultTable from "./ResultTable";

function App() {
  const [isLoading, setIsLoading] = useState(false);
  const [hasError, setHasError] = useState(false);
  const abortCtrl = useRef<AbortController>();

  const [items, setItems] = useState<Location[]>([]);

  const handleSearch = useCallback(async (query: string, type: string) => {
    // cancel running request
    if (abortCtrl.current) {
      abortCtrl.current.abort();
    }

    // reset state
    setIsLoading(false);
    setHasError(false);
    setItems([]);

    if (!query) {
      return;
    }

    setIsLoading(true);

    abortCtrl.current = new AbortController();

    const params = new URLSearchParams({ query, type });

    try {
      const res = await fetch(`/api/locations?${params}`, {
        signal: abortCtrl.current.signal,
      });

      const data = (await res.json()) as LocationResult;

      setItems(data.stations);
    } catch (err: any) {
      // don't update state on AbortController.abort()
      if (err && err.name === "AbortError") {
        return;
      }

      setHasError(true);
    }

    setIsLoading(false);
  }, []);

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        gap: "1rem",
        padding: "1rem",
      }}
    >
      <SearchBar onSearch={handleSearch} />
      {isLoading ? (
        <div style={{ color: "gray", fontStyle: "italic" }}>Loading...</div>
      ) : hasError ? (
        <div style={{ color: "darkred" }}>Could not load results.</div>
      ) : (
        <ResultTable items={items} />
      )}
    </div>
  );
}

export default App;
