import { useEffect, useState } from "react";

interface SearchBarProps {
  onSearch?: (query: string, type: string) => void;
}

function SearchBar({ onSearch }: SearchBarProps) {
  const [type, setType] = useState("all");
  const [query, setQuery] = useState("");
  const [debouncedQuery, setDebouncedQuery] = useState("");

  // debounce input value
  useEffect(() => {
    const handle = setTimeout(() => setDebouncedQuery(query), 500);
    return () => clearTimeout(handle);
  }, [query]);

  // run callback on change
  useEffect(() => {
    onSearch?.(debouncedQuery, type);
  }, [debouncedQuery, type, onSearch]);

  return (
    <div style={{ display: "flex", gap: ".5rem" }}>
      <select value={type} onChange={(ev) => setType(ev.target.value)}>
        <option value="all">All</option>
        <option value="station">Station</option>
        <option value="poi">Point Of Interest</option>
        <option value="address">Address</option>
      </select>

      <input
        autoFocus
        type="search"
        placeholder="Enter search query"
        value={query}
        onChange={(ev) => setQuery(ev.target.value)}
      />
    </div>
  );
}

export default SearchBar;
