export interface LocationResult {
  stations: Location[];
}

export interface Location {
  id: string | null;
  name: string;
}
