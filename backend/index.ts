import express from "express";
import http from "http";

const validTypes = ["all", "station", "poi", "address"];

const app = express();

app.get("/api/locations", (req, res) => {
  const query = String(req.query.query || "");
  const type = String(req.query.type || "all").toLowerCase();

  if (!validTypes.includes(type)) {
    res.sendStatus(400);
    return;
  }

  const params = new URLSearchParams({ type, query });

  http.get(`http://transport.opendata.ch/v1/locations?${params}`, (dataRes) => {
    dataRes.on("data", (data) => res.write(data));
    dataRes.on("end", () => res.end());
  });
});

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`Server started on port ${port}`));
